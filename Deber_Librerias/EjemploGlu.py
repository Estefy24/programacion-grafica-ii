#Autor:Joana Estefania Nicolalde Perugachi
#Fecha: 28/06/2020
#Ejemplo con GLU
#Descripción: Creación de un disco implementando la librería Glu

#Importamos las librerias
from OpenGL.GL import *
from OpenGL.GL import glBegin
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
from math import *

if sys.platform in ["win32", "win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
#Inicializamos
pygame.display.init()
pygame.font.init()

screen_size=[800,600]

icon=pygame.Surface((1,1))
icon.set_alpha(0)
pygame.display.set_icon(icon)

pygame.display.set_caption("Triangles")
pygame.display.set_mode(screen_size,OPENGL| DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot=[30.0,20.0]
camera_radius=3.0 #radio de la esfera
camera_center=[0.0,0.0,0.0] # el centro de la esfera

def get_input():
    global camera_rot, camera_radius
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_rel = pygame.mouse.get_rel()  # Check how much the mouse moved since you last called this function.
    for event in pygame.event.get():
        # Clicked the little "X"; close the window (return False breaks the main loop).
        if event.type == QUIT:
            return False
        # If the user pressed a key:
        elif event.type == KEYDOWN:
            # If the user pressed the escape key, close the window.
            if event.key == K_ESCAPE: return False
        # If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            # Zoom in
            if event.button == 4:
                camera_radius *= 0.9
            # Or out.
            elif event.button == 5:
                camera_radius /= 0.9
    # If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True
#Método para dibujar
def setup_draw():
    #limpiamos la pantalla
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/ float(screen_size[1]),0.1,100)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )

def draw():
    setup_draw()
    #triangle() #dibuja un triangulo
    Disco() #llama a la funcion para dibujar el disco
    pygame.display.flip()
#funcion con los vertices para la creacion del triangulo
def triangle():
    glColor3f(1,0,1)
    glBegin(GL_TRIANGLES)
    glVertex3f(2,1,0)
    glVertex3f(2, 0, 1)
    glVertex3f(0, 1, 1)
    glEnd()

#función para la creación del disco llamando a GLU
def Disco():

    disco= gluNewQuadric()
    glColor3fv((1, 1, 0.4))
    gluQuadricDrawStyle(disco, GLU_FILL)
    gluDisk(disco,0.5, 1.5, 13, 6) #base, tope, altura,slice, stack


def main():
    clock = pygame.time.Clock() #tiempo
    while True:
        if not get_input(): break
        setup_draw()
        draw() #función de dibujar
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()

#Main
if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()