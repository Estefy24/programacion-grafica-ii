#Autor: Joana Estefania Nicolalde Perugachi
# Introducción a los gráficos en Python
# Descripción: Dibujando escenario con Pygame y utilizando la librería GLU::
#Importamos librerías
import pygame
import math
import random

#Definimos los colores
AZULNOCHE = (9, 35, 67)
VERDEPASTO = (17, 99, 67)
VERDE = (10, 255, 10)
BLANCO = (222, 224, 200)
GRIS = (186, 186, 177)
GrisCastillo = (158, 158, 158)
NEGRO = (2, 3, 3)
ROJO = (255, 0, 0)
CAFE = (90, 50, 15)

#Inicializamos
pygame.init()
Dimensiones = (800, 500)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Introducción a los Gráficos")

Terminar = False
# Se define para poder gestionar cada vez que se ejecuta un fotograma
reloj = pygame.time.Clock()

while not Terminar:
    # ---Manejo de eventos
    for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True
    # ---La lógica del juego

    # ---Código de dibujo----
    Pantalla.fill(NEGRO)
    # --Todos los dibujos van después de esta línea
    pygame.draw.line(Pantalla, VERDE, [10, 10], [650, 470], 2) #dibuja una linea
    pygame.draw.rect(Pantalla, ROJO, [150, 50, 400, 400], 0) #dibuja un rectangulo
    #pygame.draw.circle(Pantalla, AZULNOCHE, [150, 50, 400, 400], 0) #dibuja un circulo
    pygame.draw.arc(Pantalla, CAFE, [150, 50, 400, 400], 0, math.pi, 2) #dibuja un arco

    pygame.draw.polygon(Pantalla, ROJO, [[350, 10], [20, 400], [680, 400]], 0) #dibuja un poligono
    pygame.draw.ellipse(Pantalla, BLANCO, [50, 50, 600, 400], 0) #dIBUJA UNA ELIPSE
    
    # --Todos los dibujos van antes de esta línea
    pygame.display.flip()
    reloj.tick(20)  # Limitamos a 20 fotogramas por segundo
pygame.quit()
