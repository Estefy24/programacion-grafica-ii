#Clase Esfera
#autor: Joana Estefania Nicolalde Perugachi
#clase para crear una esfera
class Esfera:
    #Inicializador
    def __init__(self, gl,glu):
        self.gl = gl
        self.glu = glu
    #Función de creación de la esfera con sus diferentes vértices
    def crearEsfera(self,radio,p,q,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r,g,b)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_FILL);
        self.glu.gluSphere(sphere, radio,p,q );

        self.gl.glPopMatrix()

    #Creación de la esfera en líneas (Wireframe)
    def crearEsferaWireFrame(self,radio,p,q,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_LINE);
        self.glu.gluSphere(sphere, radio, p, q);

        self.gl.glPopMatrix()