#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 26/06/2020
#python-figuras primitivas OpenGl
#Creación de un proyecto que visualice las diferentes figuras 2D y 3D implementando Luces y Texturas


import OpenGL.GL as gl
import OpenGL.GLU as glu
import numpy as np
#import pygame as p
#from Cilindro import *
from Dodecaedro import *
#from Prisma import *
from Cubo import *
#from Icosaedro import *
#from Piramide import *
#from Octaedro import *
from Esfera import *
#from Cono import *
import pygame
from pygame.locals import *

#from pygame.locals import *
import sys, os, traceback
from Textura import  *



import LUCES


#Inicializamos
if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
pygame.display.init()
pygame.font.init()
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Figuras primitivas")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)

pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

gl.glHint(gl.GL_PERSPECTIVE_CORRECTION_HINT,gl.GL_NICEST)

gl.glEnable(gl.GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0]



##Llamamos a las diferentes clases
# Creamos los objetos
#ci = Cilindro(1, 1, gl, np)
do = Dodecaedro(gl)
#pri = Prisma(gl)
cu = Cubo(gl)
#ico = Icosaedro(gl)
#pi = Piramide(gl)
#oc = Octaedro(gl)
es = Esfera(gl, glu)
#co = Cono(gl, np)

tex=Textura(gl,glu,pygame)

LUCES.IniciarIluminacion()

def get_input():
    global camera_rot, camera_radius


    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()

    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():


        if   event.type == QUIT: return False

        elif event.type == KEYDOWN:

            if   event.key == K_ESCAPE: return False

        elif event.type == MOUSEBUTTONDOWN:

            if   event.button == 4: camera_radius *= 0.9

            elif event.button == 5: camera_radius /= 0.9




    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


global varN
varN=10
#función para dibujar los elementos
def draw():
    global varN

    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)


    gl.glViewport(0, 0, screen_size[0], screen_size[1])

    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()

    glu.gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)

    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glLoadIdentity()

    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]

    glu.gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )

    #Llamamos a los métodos de las clases, de acuerdo al número que ingresamos por teclado
    Teclado()

    if varN==1: #Crea un rectángulo con la textura
        gl.glRotatef(90,0,1,0)
        tex.crearTextura("sushitex.png")

        gl.glBegin(gl.GL_QUADS)

        gl.glTexCoord2f(0, 1)
        gl.glVertex3f(-10, 10, 0)

        gl.glTexCoord2f(1, 1)
        gl.glVertex3f(10, 10, 0)

        gl.glTexCoord2f(1, 0)
        gl.glVertex3f(10, -10, 0)

        gl.glTexCoord2f(0, 0)
        gl.glVertex3f(-10, -10, 0)

        gl.glEnd()

    if (varN == 2):
        #Dodecaedro- falta implementar texturas
        do.CrearDodecaedro(1, 0, 1,1,1,1,0,0,0,0,0,0)
        do.CrearDodecaedroWireFrame(1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
    #if (varN == 3):
        #pri.crearPrisma(1, 0, 1,1,1,1,0,0,0,0,0,0)
        #pri.crearPrismaWireFrame(1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
    if (varN == 4):
        cu.crearCubo(1, 0.5, 0.8,1,1,1,0,0,0,0,0,0)
        cu.crearCuboWireFrame(1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
    #if (varN == 5):
        #ico.crearIcosaedro(1, 0.5, 0,1,1,1,0,0,0,0,0,0)
        #ico.crearIcosaedroWireframe(1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
    #if (varN == 6):
        #pi.crearPiramide(0.5, 1, 1, 1,1,1,1,0,0,0,0,0,0)
        #pi.crearPiramideWireFrame(1, 1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
    #if (varN == 7):
        #oc.crearOctaedro(1, 1, 0.4, 1,1,1,1,0,0,0,0,0,0)
        #oc.crearOctaedroWireFrame(1, 1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
    if (varN == 8):
        es.crearEsfera(1, 20, 20, 1, 0, 1,1,1,1,0,0,0,0,0,0)
        es.crearEsferaWireFrame(1, 20, 20, 1, 1, 1, 2,1,1,1,0,0,0,0,0,0)
    #if (varN == 9):
        #co.crearCono(1, 1, 1, 1, 1,1,1,1,0,0,0,0,0,0)
        #co.crearConoWireFrame(1, 1, 1, 3, 1, 1,1,1,1,0,0,0,0,0,0)



    pygame.display.flip()
    #gl.glDeleteTextures(tex.texture_id)

#Función del teclado
def Teclado():
    global  varN

    teclado = pygame.key.get_pressed()

    if teclado[K_1]:
        pygame.display.set_caption('Cilindro')
        varN = 1

    if teclado[K_2]:
        pygame.display.set_caption('Dodecaedro')
        varN = 2

    if teclado[K_3]:
        pygame.display.set_caption('Prisma')
        varN = 3

    if teclado[K_4]:
        pygame.display.set_caption('Cubo')
        varN = 4

    if teclado[K_5]:
        pygame.display.set_caption('Icosaedro')
        varN = 5

    if teclado[K_6]:
        pygame.display.set_caption('Piramide')
        varN = 6

    if teclado[K_7]:
        pygame.display.set_caption('Octaedro')
        varN = 7

    if teclado[K_8]:
        pygame.display.set_caption('Esfera')
        varN = 8

    if teclado[K_9]:
        pygame.display.set_caption('Cono')
        varN = 9


def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()
        clock.tick(60)
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()



