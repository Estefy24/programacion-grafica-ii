
#Autor:Joana Estefanía Nicolalde Perugachi
#Clase para implementar una textura
#Descripción: Una clase para crear una textura con todos los parámetros.Se la llama del Main
class Textura:
    global texture_id
   #Inicializador
    def __init__(self, gl,glu,pygame):
        self.gl = gl
        self.glu = glu
        self.pygame=pygame
    #Función de creación de textura con parámetro de imagen
    def crearTextura(self, imagen):
        # Load the textures
        global texture_id
        texture_surface = self.pygame.image.load(imagen)
        # Retrieve the texture data
        texture_data = self.pygame.image.tostring(texture_surface, 'RGB', True)

        # Generate a texture id
        texture_id = self.gl.glGenTextures(1)
        # Tell OpenGL we will be using this texture id for texture operations
        self.gl.glBindTexture(self.gl.GL_TEXTURE_2D, texture_id)

        # Tell OpenGL how to scale images
        self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MAG_FILTER, self.gl.GL_LINEAR)
        self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MIN_FILTER, self.gl.GL_LINEAR)

        # Tell OpenGL that data is aligned to byte boundries
        self.gl.glPixelStorei(self.gl.GL_UNPACK_ALIGNMENT, 1)

        # Get the dimensions of the image
        width, height = texture_surface.get_rect().size

        # gluBuild2DMipmaps
        self.glu.gluBuild2DMipmaps(self.gl.GL_TEXTURE_2D,
                          3,
                          width,
                          height,
                          self.gl.GL_RGB,
                          self.gl.GL_UNSIGNED_BYTE,
                          texture_data)

