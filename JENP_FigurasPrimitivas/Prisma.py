#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 26/06/2020
#Clase Prisma
#Descripción: Una clase que mantiene todos los componentes necesarios para la creación del prisma y sus diferentes transformaciones
#Es llamada por la clase Main.
class Prisma:
    #Inicializador
    def __init__(self, gl):
        self.gl = gl

    #Para crear el prisma Wireframe con sus diferentes vértices
    def crearPrismaWireFrame(self,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s) #tamaño de línea

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glPopMatrix()
    #Función que crea el prisma con sus vértices
    def crearPrisma(self, r, g, b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.3, 0.5)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glColor3f(0.3 + g, 0.5, 0.5)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.5, 0.5)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.5, 0.5)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.6, 0.6)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.6, 0.6)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.6, 0.6)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(-1.0, -1.0, 0.0)
        self.gl.glEnd()
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.6, 0.6)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(1.0, 1.0, 1.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(1.0, -1.0, 1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(0.3 + r, 0.6, 0.6)
        self.gl.glVertex3f(-1.0, 1.0, 0.0)
        self.gl.glColor3f(0.3, 0.5, 0.5 + b)
        self.gl.glVertex3f(1.0, 1.0, -1.0)
        self.gl.glColor3f(0.3 + g, 0.3, 0.5)
        self.gl.glVertex3f(1.0, -1.0, -1.0)
        self.gl.glEnd()

        self.gl.glPopMatrix()



