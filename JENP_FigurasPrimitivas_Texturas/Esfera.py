#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 26/06/2020
#Clase Esfera
#Descripción: Una clase que mantiene todos los componentes necesarios para la creación de la esfera y sus diferentes transformaciones
#Es llamada por la clase Main.
class Esfera:
    #Inicializador
    def __init__(self, gl,glu):
        self.gl = gl
        self.glu = glu
    #Método para crear la esfera
    def crearEsfera(self,radio,p,q,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r,g,b)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_FILL);
        #self.gl.glTexCoord2f() #No tengo los vertices :c
        self.glu.gluSphere(sphere, radio,p,q );

        self.gl.glPopMatrix()
    #Método para crear la esfera de alambre (WireFrame)
    def crearEsferaWireFrame(self,radio,p,q,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez) #Escala
        self.gl.glTranslate(tx, ty, tz) #Translada
        self.gl.glRotate(rx, 1, 0, 0)#Rota en x
        self.gl.glRotate(ry, 0, 1, 0)#Rota en y
        self.gl.glRotate(rz, 0, 0, 1)#Rota en z

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_LINE);
        self.glu.gluSphere(sphere, radio, p, q);

        self.gl.glPopMatrix()