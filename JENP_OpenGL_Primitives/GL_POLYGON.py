#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 24/06/2020
#Dibujar UN polygon con OpenGL
#Descripción: Mediante GL_POLYGON y glVertex3f- Implementación de luces
import traceback
from GL_TRIANGLES import get_input, setup_draw
import pygame
from OpenGL.GL import *
import GL
import LUCES


pygame.display.set_caption("Polygon")
LUCES.IniciarPantalla([600,600])
LUCES.IniciarIluminacion()
def polygon():
    #Creacion de un poligono
    glBegin(GL_POLYGON)
    glVertex3f(0, 0, 0)
    glVertex3f(3, 3, 0)
    glVertex3f(2, 0, 0)
    glVertex3f(0, 2, 0)
    glVertex3f(2, 0, 0)
    glEnd()

#Método para dibujar
def draw():
    polygon()
    pygame.display.flip()


def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        setup_draw()
        draw()
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()