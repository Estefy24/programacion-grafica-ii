#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 24/06/2020
#Dibujar  las diferentes figuras básicas tridimensionales en OpenGL
#Creación de un círculo dentro de un cilindro con GLU

#importamos las bibliotecas
#GLU es una minibiblioteca de OpenGL
import pygame
import OpenGL
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math



#Ahora creamos una función para llamar al círculo
def Circulo():
#para crear las caras
    radio=0.8
    glBegin(GL_POLYGON)
    for i in range(0,1000):
        glColor3fv((0.5, 1, 0.4))
        a = (math.cos(i) * radio)
        b = (radio * math.sin(i))
        glVertex2f(a , b )
    glEnd()
#Método para crear un cilindro
def Cilindro():
    cylinder = gluNewQuadric();
    glColor3fv((1, 1, 0.4))
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    gluCylinder(cylinder, 1, 1, 2, 25, 25);


#Ahora definimos el Main
def main():
    pygame.init()
    display = (800,600) #damos la dimensión de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0) #determina la perspectiva (campo de visión, relación de aspecto, cerca,lejos)
    #Transformación, retrocedemos cinco unidades para poder ver el círculo
    glTranslatef(0.0,0.0, -5)

#Bucle de eventos para pygame
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(2, 2, 2, 2) #Transformación de rotación
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) #limpiamos la pantalla
        Circulo() #llamamos a cubo
        Cilindro()#llamamos a cilindro
        pygame.display.flip() #actualiza la pantalla
        pygame.time.wait(10)# espera de tiempo de 10 segundos
main()
