#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 24/06/2020
#Dibujar  las diferentes figuras básicas tridimensionales en OpenGL
#Creación de un Cubo a colores

#importamos las bibliotecas
#GLU es una minibiblioteca de OpenGL
import pygame
import OpenGL
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *


#creamos vértices en (x,y,z), en un cubo hay 8 vértices
vertices= (
    (1, -1, -1),
    (1, 1, -1),
    (-1, 1, -1),
    (-1, -1, -1),
    (1, -1, 1),
    (1, 1, 1),
    (-1, -1, 1),
    (-1, 1, 1)
    )
#creamos los bordes
edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
    )

#creamos las caras
surfaces = (
(0,1,2,3),
(3,2,7,6),
(6,7,5,4),
(4,5,1,0),
(1,5,7,2),
(4,0,3,6)


)


colors = (
(1,0,1),
(0,1,1),
(1,0,1),
(0,1,1),
(1,0.5,1),
(0,0,1),
(0.1,0,1),
(0,1,1),
(1,0.7,0),
(0,0,0.4),
(0.3,0,1),
(1,0,0.7),
)

#Cada una de las tuplas anteriores contiene dos números.
# Esos números corresponden a un vértice, y el "borde" se dibujará entre esos dos vértices.
# Comenzamos con 0, ya que así es como funcionan Python y la mayoría de los lenguajes de programación (el primer elemento es 0).
# Entonces, 0 corresponde al primer vértice que definimos (1, -1, -1) ... y así sucesivamente.

#Ahora creamos una función para llamar al cubo
def Cubo():
#para crear las caras
    glBegin(GL_QUADS)
    x=0
    for surface in surfaces:
        x+=1
        #coloca un solo color al cubo
        #glColor3fv((1,0.5,0.4))
        #coloca los colores en las diferentes caras
        glColor3fv(colors[x])
        for vertex in surface:
            glVertex3fv(vertices[vertex])
    glEnd()


    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glColor3fv((1, 1, 0.4))
            glVertex3fv(vertices[vertex])
    glEnd()

#Ahora definimos el main
def main():
    pygame.init()
    display = (800,600) #damos la dimensión de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0) #determina la perspectiva (campo de visión, relación de aspecto, cerca,lejos)
    #Transformación, retrocedemos cinco unidades para poder ver el cubo
    glTranslatef(0.0,0.0, -5)

#Bucle de eventos para pygame
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(2, 2, 2, 2) #Transformación de rotación
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) #limpiamos la pantalla
        Cubo() #llamamos a cubo
        pygame.display.flip() #actualiza la pantalla
        pygame.time.wait(10)# espera de tiempo de 10 segundos
main()
