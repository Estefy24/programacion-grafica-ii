#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 24/06/2020
#Dibujar  las diferentes figuras básicas 2D en OpenGL
#Creación de un escenario de una casa con las figuras básicas

#importamos las bibliotecas
#GLU es una minibiblioteca de OpenGL
import pygame
import OpenGL
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import math



#Ahora creamos una función para llamar al círculo
def Esfera():
#para crear las caras
    radio=0.3

    glBegin(GL_POLYGON)
    for i in range(0,1000):
        glColor3fv((1, 1, 0))
        a = (math.cos(i) * radio)
        b = (radio * math.sin(i))
        glVertex2f(a+2.8 , b+3.6)
    glEnd()



def Rectangulo(color):
    glBegin(GL_QUADS)

    glColor3fv(color)
    glVertex2f(0,0)
    glVertex2f(6, 0)
    glVertex2f(6, 1)
    glVertex2f(0, 1)
    glEnd()

def Triangulo(color):
    glBegin(GL_TRIANGLES)
    glColor3fv(color)
    glVertex2f(0, 0)
    glVertex2f(6, 0)
    glVertex2f(3, 1)
    glEnd()

def Casa():
    #parte baja de la casa
    glPushMatrix()
    glScalef(0.3,1.5,1)
    glTranslatef(0.5,0.67, 0)
    Rectangulo((1,0.1,0.5))
    glPopMatrix()
    #techo
    glPushMatrix()
    glScalef(0.3, 1.5, 1)
    glTranslatef(0.5, 1.67, 0)
    Triangulo((1,0.4,0.7))
    glPopMatrix()
    #ventanas
    #izquierda
    glPushMatrix()
    glScalef(0.1, 0.5, 1)
    glTranslatef(2.5, 3.67, 0)
    Rectangulo((0, 0, 0.5))
    glPopMatrix()
    #derecha
    glPushMatrix()
    glScalef(0.1, 0.5, 1)
    glTranslatef(12.5, 3.67, 0)
    Rectangulo((0, 0, 0.5))
    glPopMatrix()

    #puerta
    glPushMatrix()
    glScalef(0.1, 0.8, 1)
    glTranslatef(8, 1.24, 0)
    Rectangulo((0, 0.5, 0.5))
    glPopMatrix()


def Arbol():
    glPushMatrix()
    glScalef(0.08, 1, 1)
    glTranslatef(31.5, 1, 0)
    Rectangulo((0.6, 0.1, 0.1))
    glPopMatrix()
    #parte superior
    glPushMatrix()
    glScalef(0.2, 1, 1)
    glTranslatef(11, 2, 0)
    Triangulo((0, 1, 0.5))
    glPopMatrix()

    glPushMatrix()
    glScalef(0.2, 1, 1)
    glTranslatef(11, 1.8, 0)
    Triangulo((0, 1, 0.5))
    glPopMatrix()

    glPushMatrix()
    glScalef(0.2, 1, 1)
    glTranslatef(11, 1.6, 0)
    Triangulo((0, 1, 0.5))
    glPopMatrix()


#Ahora definimos el main
def main():
    pygame.init()
    display = (800,600) #damos la dimensión de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0) #determina la perspectiva (campo de visión, relación de aspecto, cerca,lejos)
    #Transformación, retrocedemos cinco unidades para poder ver el círculo
    glTranslatef(-2.8,-2.1, -5)

#Bucle de eventos para pygame
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        #glRotatef(2, 2, 2, 2) #Transformacion de rotacion
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) #limpiamos la pantalla
        #sol
        Esfera() #llamamos a esfera
        #cesped
        Rectangulo((0.1,1,0.5)) # llamamos al rectangulo
        #casa
        Casa()
        #segunda casa
        glPushMatrix()
        glTranslatef(3.5,0,0)
        Casa()
        glPopMatrix()
        #arbol
        Arbol()
        pygame.display.flip() #actualiza la pantalla
        pygame.time.wait(10)# espera de tiempo de 10 segundos
main()
