#Autor: Joana Estefanía Nicolalde Perugachi
#Fecha: 24/06/2020
#Dibujar  las diferentes figuras básicas tridimensionales en OpenGL
#Creación de un tetraedro a colores
import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

verticies = (
    (1, -1, -1),
    (1, 1, -1),
    (-1, 1, -1),
    (-1, -1, -1),
    (1, -1, 1),
    (1, 1, 1),
    (-1, -1, 1),
    (-1, 1, 1)
    )


#triángulo
edges = (
   (4,0),
   (4,1),
   (4,2),
   (4,3),
   (0,1),
   (0,3),
   (2,1),
   (2,3),
   )
#definimos las caras
surfaces = (
(0,1,2,3),
(1,1,4,2),
(1,1,4,0),
(0,0,3,4),
(2,2,1,4),

)


colors = (
(1,1,1),
(0.5,1,0.5),
(1,0.8,0.8),
(0.5,0.5,1),
(1,0.5,1),
(0.5,0.8,0),

)


def Triangulo():
    glBegin(GL_QUADS)
    x = 0
    for surface in surfaces:
        x += 1
        # coloca un solo color al cubo
       # glColor3fv((1, 0.5, 0.4))
        # coloca los colores en las diferentes caras
        glColor3fv(colors[x])
        for vertex in surface:
            glVertex3fv(verticies[vertex])
    glEnd()

#dibuja las líneas de la pirámide
    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glColor3fv((1, 1, 0.4))
            glVertex3fv(verticies[vertex])
    glEnd()

#definimos el Main
def main():
    pygame.init()
    display = (800,600) #resolución de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)

    glTranslatef(0.0,0.0, -5)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        Triangulo() #llamamos a la función triángulo
        pygame.display.flip()
        pygame.time.wait(10)

#visual python
main()