#Separación del proyecto con iluminación de Ian Mallet
#Modificado por: Joana Estefanía Nicolalde Perugachi
from OpenGL.GL import *
import OpenGL.GLU as glu
import numpy as np
import pygame
from pygame.locals import *
import sys, os, traceback
#Para la implementacion de luces

from PIL import Image as Image

from glLib import *

import basiclighting
from glLib.glLibLight import glLibLight
from glLib.glLibLocals import GLLIB_POINT_LIGHT
from glLib.glLibObjects import glLibPlane, glLibObject
from glLib.glLibShader import glLibShader, glLibUseShader
from glLib.glLibView import glLibView3D

if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
#Inicializacion
pygame.display.init()
pygame.font.init()
screen_size = [1000,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Figuras primitivas")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)

pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)

glEnable(GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0]

global View3D, Plane, CameraRotation, CameraRadius, Light1, Light2, Light3, Light4, Shader, Normalmap, Object
global dl
#View3D = glLibView3D((0, 0, Screen[0], Screen[1]), 45, 0.1, 20)

# A plane of size 5, with a normal facing straight up
# i.e., the XZ plane
Plane = glLibPlane(5, (0, 1, 0))

# A object, loaded as before
Object = glLibObject("trisphere.obj")
Object.build_list()

# Add variables for the camera's rotation and radius
CameraRotation = [90, 23]
CameraRadius = 8.0

glEnable(GL_LIGHTING)

# Make a light object
Light1 = glLibLight(1)
# Set the light's position
Light1.set_pos([0, 1.8, 0])
# Make it a point light (as opposed to directional)
Light1.set_type(GLLIB_POINT_LIGHT)

Light1.set_atten(1.0, 0.0, 1.0)

Light1.enable()

Light2 = glLibLight(2)
Light2.set_pos([2, 0.1, 0])
Light2.set_type(GLLIB_POINT_LIGHT)

Light2.set_diffuse([0, 0, 1])
Light2.set_specular([0, 0, 1])
Light2.set_atten(1.0, 0.0, 1.0)
Light2.enable()

Light3 = glLibLight(3)
Light3.set_pos([-2, 1.0, -2.5])
Light3.set_type(GLLIB_POINT_LIGHT)
Light3.set_diffuse([0, 1, 0])
Light3.set_specular([0, 1, 0])
Light3.set_atten(1.0, 0.0, 1.0)

Light3.set_spot_dir(normalize([0.2, -1.0, 1.0]))
Light3.set_spot_angle(45.0)
Light3.set_spot_ex(0.5)
Light3.enable()

Light4 = glLibLight(4)
Light4.set_pos([-0.5, 1.0, -2.5])
Light4.set_type(GLLIB_POINT_LIGHT)
Light4.set_diffuse([1, 0, 0])
Light4.set_specular([1, 0, 0])
Light4.set_atten(1.0, 0.0, 1.0)
Light4.set_spot_dir(normalize([-0.4, -1.0, 1.0]))
Light4.set_spot_angle(45.0)
Light4.set_spot_ex(0.5)
Light4.enable()

pygame.mouse.get_rel()

dl = glGenLists(1)
glNewList(dl, GL_COMPILE)
glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, [1, 1, 0.5, 1])
glBegin(GL_QUADS)
glNormal3f(0, 1, 0)
glVertex3f(1, 0, 0)
glVertex3f(2, 0, 0)
glVertex3f(2, 0, 1)
glVertex3f(1, 0, 1)
glEnd()
glEndList()

Shader = glLibShader()

Shader.render_equation("""
   vec3 ambient_light = light_ambient(light1).rgb+
                        light_ambient(light2).rgb+
                        light_ambient(light3).rgb+
                        light_ambient(light4).rgb;

   vec3 diffuse_light = light_diffuse(light1).rgb*light_att(light1)+
                        light_diffuse(light2).rgb*light_att(light2)+
                    2.0*light_diffuse(light3).rgb*light_att(light3)*light_spot(light3)+
                    2.0*light_diffuse(light4).rgb*light_att(light4)*light_spot(light4);

   vec3 specular_light = light_specular_ph(light1).rgb*light_att(light1)+
                         light_specular_ph(light2).rgb*light_att(light2)+
                     2.0*light_specular_ph(light3).rgb*light_att(light3)*light_spot(light3)+
                     2.0*light_specular_ph(light4).rgb*light_att(light4)*light_spot(light4);

   color.rgb += ambient_color.rgb*ambient_light;
   color.rgb += diffuse_color.rgb*diffuse_light;
   color.rgb += specular_color.rgb*specular_light;

   //color.rgb *= vec3(0.0,0.0,1.0);""")
errors = Shader.compile()
print(errors)




















##Llamamos a las diferentes clases
# Creamos los objetos


global varX
varX=0

global varY
varY=5

global varZ
varZ=0

def get_input():
    global camera_rot, camera_radius


    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()

    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():


        if   event.type == QUIT: return False

        elif event.type == KEYDOWN:

            if   event.key == K_ESCAPE: return False

        elif event.type == MOUSEBUTTONDOWN:

            if   event.button == 5: camera_radius *= 0.2

            elif event.button == 4: camera_radius /= 0.2




    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True





#Variable global para el teclado
global varN
varN=10
global textID

global cont
cont=0



def quit():
    global Light1, Light2, Light3, Light4
    gl.glDisable(gl.GL_LIGHTING)
    del Light1, Light2, Light3, Light4


def draw():
    global varN, varX, varY, varZ, cont
    global textID

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


    glViewport(0, 0, screen_size[0], screen_size[1])

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    glu.gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]

    glu.gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )
    Lineas()

    Light1.set()
    Light2.set()
    Light3.set()
    Light4.set()

    # Draw the floor
    #   Use the shader
    glLibUseShader(Shader)

    #   Save the current material, and then use
    #   a material that maximizes all parameters
    #   Then, lower the specular exponent to
    #   simulate a less shiny surface.
    ##    glPushAttrib(GL_LIGHTING_BIT)
    ##    glLibUseMaterial(GLLIB_MATERIAL_FULL)
    ##    glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,5.0)

    #   Draw the plane
    Plane.draw()
    ##    glBegin(GL_QUADS)
    ##    glVertex3f(0,0,0)
    ##    glVertex3f(1,0,0)
    ##    glVertex3f(1,0,1)
    ##    glVertex3f(0,0,1)
    ##    glEnd()

    #   Draw the object
    glPushMatrix()
    #       Translate it up.  Enclose in push and pop so that this
    #       transformation doesn't affect subsequent operations
    glTranslatef(0, 1, 0)
    #       Draw the object and restore modelview matrix
    ##    glLibUseShader(None)
    ##    glLibUseShader(Shader)
    ##    Object.draw_list()
    glCallList(dl)
    glPopMatrix()

    #   Restore the default material
    ##    glPopAttrib()

    #   Use the fixed function pipeline again
    glLibUseShader(None)

    # Draw the lights as points
    for light in [Light1, Light2, Light3, Light4]:
        light.draw_as_point()

    pygame.display.flip()

#Funcion del teclado
def Luz():
    global varX, varY, varZ
    glPushMatrix()
    glTranslate(varX,varY,varZ)
    sphere = glu.gluNewQuadric();
    glColor3f(1, 1, 1)
    glu.gluQuadricDrawStyle(sphere, glu.GLU_FILL);
    glu.gluSphere(sphere, 0.3, 20, 20);
    glPopMatrix()

def Teclado():
    global  varN, varX, varY, varZ

    teclado = pygame.key.get_pressed()

    if teclado[K_1]:
        #Cambia el nombre de la ventana
        pygame.display.set_caption('Cilindro')
        varN = 1


    if teclado[K_2]:
        pygame.display.set_caption('Dodecaedro')
        varN = 2

    if teclado[K_3]:
        pygame.display.set_caption('Prisma')
        varN = 3

    if teclado[K_4]:
        pygame.display.set_caption('Cubo')
        varN = 4

    if teclado[K_5]:
        pygame.display.set_caption('Icosaedro')
        varN = 5

    if teclado[K_6]:
        pygame.display.set_caption('Piramide')
        varN = 6

    if teclado[K_7]:
        pygame.display.set_caption('Octaedro')
        varN = 7

    if teclado[K_8]:
        pygame.display.set_caption('Esfera')
        varN = 8

    if teclado[K_9]:
        pygame.display.set_caption('Cono')
        varN = 9

    if teclado[K_RIGHT]:
        varX=varX+0.05

    if teclado[K_LEFT]:
        varX=varX-0.05

    if teclado[K_UP]:
        varY=varY+0.05

    if teclado[K_DOWN]:
        varY=varY-0.05

    if teclado[K_z]:
        varZ=varZ+0.05

    if teclado[K_a]:
        varZ=varZ-0.05


def Lineas():
    glBegin(GL_LINES)
    # Change the color to red.  All subsequent geometry we draw will be red.
    glColor3f(1, 0, 0)
    # Make two vertices, thereby drawing a (red) line.
    glVertex(0, 0, 0)
    glVertex3f(5, 0, 0)
    # Change the color to green.  All subsequent geometry we draw will be green.
    glColor3f(0, 1, 0)
    # Make two vertices, thereby drawing a (green) line.
    glVertex(0, 0, 0)
    glVertex3f(0, 5, 0)
    # Change the color to blue.  All subsequent geometry we draw will be blue.
    glColor3f(0, 0, 1)
    # Make two vertices, thereby drawing a (blue) line.
    glVertex(0, 0, 0)
    glVertex3f(0, 0, 5)
    glEnd()

def main():
    #global  texture_id
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()

        clock.tick(60)

    pygame.quit()






if __name__ == "__main__":
    try:
        main()


    except:
        traceback.print_exc()
        pygame.quit()
        input()
