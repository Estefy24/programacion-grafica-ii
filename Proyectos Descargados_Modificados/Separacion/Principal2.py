#Separación del proyecto con iluminación y Shaders  con texturas de Ian Mallet
#Modificado por: Joana Estefanía Nicolalde Perugachi

from OpenGL.GL import *
import OpenGL.GLU as glu
import numpy as np
import pygame
from pygame.locals import *
import sys, os, traceback
#Para la implementacion de luces

from PIL import Image as Image

from glLib import *

import basiclighting
from glLib.glLibLight import glLibLight
from glLib.glLibLocals import GLLIB_POINT_LIGHT
from glLib.glLibObjects import glLibPlane, glLibObject
from glLib.glLibShader import glLibShader, glLibUseShader
from glLib.glLibView import glLibView3D

if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
#Inicializacion
pygame.display.init()
pygame.font.init()
screen_size = [1000,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Figuras primitivas")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)

pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)

glEnable(GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0]

global View3D, Plane, CameraRotation, CameraRadius, Light1, Light2, Light3, Light4, Shader, Normalmap, Object
global dl
#View3D = glLibView3D((0, 0, Screen[0], Screen[1]), 45, 0.1, 20)



global InsetView2D, View3D, CubemapView
global Objects, Plane, BoundingCube, UnwrappedMap, Cubemap
global ObjectRotation, CameraRotation, CameraRadius
global Light1
global Shader

cubemapsize = 512
InsetView2D = glLibView2D((0, 0, 300, 300))
#View3D = glLibView3D((0, 0, Screen[0], Screen[1]), 45, 0.1, 200)
# Cubemap view.  Angle must be 90 degrees.
CubemapView = glLibView3D((0, 0, cubemapsize, cubemapsize), 90, 1.5, 10.0)

Objects = []
for path in ["Spaceship.obj", "trisphere.obj", "cube.obj", "icosahedron.obj", "disco.obj", "tetrahedron.obj"]:
    object = glLibObject("objects/" + path, GLLIB_FILTER, GLLIB_MIPMAP_BLEND)
    ##        object.set_material([[-1,-1,-1,1.0],0,-1]) #give the object a soft shininess, leave the other material parameters alone.
    object.build_list([1])
    Objects.append(object)

#box_tex = glLibTexture2D("floor.jpg", GLLIB_ALL, GLLIB_RGB, GLLIB_FILTER, GLLIB_MIPMAP_BLEND)
box_tex = glLibTexture2D("texturaDesierto1.png", GLLIB_ALL, GLLIB_RGB, GLLIB_FILTER, GLLIB_MIPMAP_BLEND)

if GLLIB_ANISOTROPY_AVAILABLE: box_tex.anisotropy(GLLIB_MAX)
BoundingCube = glLibRectangularSolid([5.0, 5.0, 5.0], texture=[box_tex] * 6, normalflip=True)

Cubemap = glLibTextureCube([None] * 6, (0, 0, cubemapsize, cubemapsize), GLLIB_DEPTH)
Cubemap.edge(GLLIB_CLAMP)

# For the unwrapped version of the cubemap
faces = [[[None, GLLIB_TOP, None, None],
          [GLLIB_RIGHT, GLLIB_FRONT, GLLIB_LEFT, GLLIB_BACK],
          [None, GLLIB_BOTTOM, None, None]],
         [[None, [-1, False, False], None, None],
          [[1, False, False], [1, False, False], [-1, False, False], [1, False, False]],
          [None, [1, False, False], None, None]]]
size = 64
UnwrappedMap = glLibUnwrappedCubemap(faces, size)

ObjectRotation = [0, 0]
CameraRotation = [90, 23]
CameraRadius = 15.0
##    CameraRadius = 35.0

glEnable(GL_LIGHTING)
Light1 = glLibLight(1)
Light1.set_pos([0, 0, 0])
Light1.set_type(GLLIB_POINT_LIGHT)
Light1.enable()


pygame.mouse.get_rel()

Shader = glLibShader()
Shader.render_equation("""
    color.rgb += ambient_color.rgb*light_ambient(light1).rgb;
    color.rgb += diffuse_color.rgb*light_diffuse(light1).rgb;
    color.rgb += specular_color.rgb*light_specular_ph(light1).rgb;

    color.rgb *= texture2D(tex2D_1,uv).rgb;
    float shadowed = cubemap_depthtest(texCube_2,vec3(0.0),""" + str(float(CubemapView.near)) + "," + str(
    float(CubemapView.far)) + """);//light1.position.xyz
    color.rgb *= clamp(shadowed,0.5,1.0);""")
Shader.max_textures_cube(2)
errors = Shader.compile()
print(errors)


##Llamamos a las diferentes clases
# Creamos los objetos

global varX
varX=0

global varY
varY=5

global varZ
varZ=0

def get_input():
    global camera_rot, camera_radius


    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()

    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():


        if   event.type == QUIT: return False

        elif event.type == KEYDOWN:

            if   event.key == K_ESCAPE: return False

        elif event.type == MOUSEBUTTONDOWN:

            if   event.button == 5: camera_radius *= 0.2

            elif event.button == 4: camera_radius /= 0.2




    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True





#Variable global para el teclado
global varN
varN=10
global textID

global cont
cont=0


def send_matrix_transform(shader,func):
    glPushMatrix()
    glLoadIdentity()
    func()
    shader.pass_mat4("transform_matrix",glGetFloatv(GL_MODELVIEW_MATRIX))
    glPopMatrix()


def draw_occluders(shader=False):
    Light1.set()
    def transform(pos):
        glTranslatef(*pos)
        glRotatef(ObjectRotation[0],0,1,0)
        glRotatef(ObjectRotation[1],1,0,0)
    index = 0
    for pos in [[0,0,3],[0,0,-3],[3,0,0],[-3,0,0],[0,3,0],[0,-3,0]]:
        glPushMatrix()
        transform(pos)
        #if the cube shadowmap drawing shader is supplied,
        #send the necessary transforms to it.
        if shader != False:
            send_matrix_transform(shader,lambda:transform(pos))
        Objects[index].draw_list()
        glPopMatrix()
        index += 1
    if shader == False:
        BoundingCube.draw()
    else:
        glEnable(GL_CULL_FACE)
        send_matrix_transform(Shader,lambda:0)
        BoundingCube.draw()
        glDisable(GL_CULL_FACE)





def quit():
    global Light1, Light2, Light3, Light4
    gl.glDisable(gl.GL_LIGHTING)
    del Light1, Light2, Light3, Light4


def draw():
    global varN, varX, varY, varZ, cont
    global textID

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(1.0, 1.0)
    glLibUpdateCubeMap(Cubemap, CubemapView, Light1.get_pos(), draw_occluders, format=GLLIB_DEPTH, update=GLLIB_ALL)
    glPolygonOffset(0.0, 0.0);
    glDisable(GL_POLYGON_OFFSET_FILL)


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


    glViewport(0, 0, screen_size[0], screen_size[1])

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    glu.gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]

    glu.gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )
    Lineas()

    Light1.set()

    Light1.draw_as_point(10)

    glLibUseShader(Shader)
    Shader.pass_texture(Cubemap,2)
    draw_occluders(Shader)
    glLibUseShader(None)

    #Light2.set()
    #Light3.set()
    #Light4.set()

    # Draw the floor
    #   Use the shader
    #glLibUseShader(Shader)

    #   Save the current material, and then use
    #   a material that maximizes all parameters
    #   Then, lower the specular exponent to
    #   simulate a less shiny surface.
    ##    glPushAttrib(GL_LIGHTING_BIT)
    ##    glLibUseMaterial(GLLIB_MATERIAL_FULL)
    ##    glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,5.0)

    #   Draw the plane
    #Plane.draw()
    ##    glBegin(GL_QUADS)
    ##    glVertex3f(0,0,0)
    ##    glVertex3f(1,0,0)
    ##    glVertex3f(1,0,1)
    ##    glVertex3f(0,0,1)
    ##    glEnd()

    #   Draw the object


    #   Restore the default material
    ##    glPopAttrib()

    #   Use the fixed function pipeline again
    InsetView2D.set_view()
    glEnable(GL_SCISSOR_TEST)
    glScissor(0, 0, InsetView2D.width, InsetView2D.height)
    #Window.clear()
    glDisable(GL_LIGHTING)
    UnwrappedMap.draw()
    glEnable(GL_LIGHTING)
    glDisable(GL_SCISSOR_TEST)




    pygame.display.flip()

#Funcion del teclado
def Luz():
    global varX, varY, varZ
    glPushMatrix()
    glTranslate(varX,varY,varZ)
    sphere = glu.gluNewQuadric();
    glColor3f(1, 1, 1)
    glu.gluQuadricDrawStyle(sphere, glu.GLU_FILL);
    glu.gluSphere(sphere, 0.3, 20, 20);
    glPopMatrix()

def Teclado():
    global  varN, varX, varY, varZ

    teclado = pygame.key.get_pressed()

    if teclado[K_1]:
        #Cambia el nombre de la ventana
        pygame.display.set_caption('Cilindro')
        varN = 1


    if teclado[K_2]:
        pygame.display.set_caption('Dodecaedro')
        varN = 2

    if teclado[K_3]:
        pygame.display.set_caption('Prisma')
        varN = 3

    if teclado[K_4]:
        pygame.display.set_caption('Cubo')
        varN = 4

    if teclado[K_5]:
        pygame.display.set_caption('Icosaedro')
        varN = 5

    if teclado[K_6]:
        pygame.display.set_caption('Piramide')
        varN = 6

    if teclado[K_7]:
        pygame.display.set_caption('Octaedro')
        varN = 7

    if teclado[K_8]:
        pygame.display.set_caption('Esfera')
        varN = 8

    if teclado[K_9]:
        pygame.display.set_caption('Cono')
        varN = 9

    if teclado[K_RIGHT]:
        varX=varX+0.05

    if teclado[K_LEFT]:
        varX=varX-0.05

    if teclado[K_UP]:
        varY=varY+0.05

    if teclado[K_DOWN]:
        varY=varY-0.05

    if teclado[K_z]:
        varZ=varZ+0.05

    if teclado[K_a]:
        varZ=varZ-0.05


def Lineas():
    glBegin(GL_LINES)
    # Change the color to red.  All subsequent geometry we draw will be red.
    glColor3f(1, 0, 0)
    # Make two vertices, thereby drawing a (red) line.
    glVertex(0, 0, 0)
    glVertex3f(5, 0, 0)
    # Change the color to green.  All subsequent geometry we draw will be green.
    glColor3f(0, 1, 0)
    # Make two vertices, thereby drawing a (green) line.
    glVertex(0, 0, 0)
    glVertex3f(0, 5, 0)
    # Change the color to blue.  All subsequent geometry we draw will be blue.
    glColor3f(0, 0, 1)
    # Make two vertices, thereby drawing a (blue) line.
    glVertex(0, 0, 0)
    glVertex3f(0, 0, 5)
    glEnd()

def main():
    #global  texture_id
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()

        clock.tick(60)

    pygame.quit()






if __name__ == "__main__":
    try:
        main()


    except:
        traceback.print_exc()
        pygame.quit()
        input()
