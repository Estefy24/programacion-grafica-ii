#Proyecto descargado de los ejemplos de la libreria PYOPENGL, de libre uso
#Modificado por: Joana Estefanía Nicolalde Perugachi


from Classes import *
from Maths import *
import SceneLoad
import sys, random
import pygame
pygame.init()
def TrueFalseCastRay(CastingRay):
    for o in Objects:
        Dist = CollideSphere(CastingRay,o)
        if Dist != False:
            return True
    return False
def CastRay(CastingRay):
    global depth
    depth += 1
    if depth == 10:
        return (255,0,255)
    Color = [0,0,0]
    ObjectNumber = 0
    Collisions = []
    for o in Objects:
        Dist = CollideSphere(CastingRay,o)
        if Dist != False:
            Collisions.append([Dist,ObjectNumber])
        ObjectNumber += 1
    if Collisions == []:
        return BackgroundColor
    else:
        #Find the closest collision point, and get the object the ray hit.
        ObjectCollision = min(Collisions)
        obj = Objects[ObjectCollision[1]]
        CollisionPoint = DistToPoint(CastingRay,ObjectCollision[0])
        n = Normalize(VectorSubtract(CollisionPoint,obj.pos))
        #Add the object's color to the current color
        Color[0] += obj.mat.color[0]*(1.0-obj.mat.reflect)
        Color[1] += obj.mat.color[1]*(1.0-obj.mat.reflect)
        Color[2] += obj.mat.color[2]*(1.0-obj.mat.reflect)
        if Reflection:
            #Form the reflection ray.
            d = CastingRay.dir
            direction = VectorSubtract(d,ScalarVector(2*DotProduct(n,d),n))
            CastingRay2 = Ray(CollisionPoint,direction)
            #Cast the reflection ray
            Color2 = CastRay(CastingRay2)
            #Add the reflection to the current color.
            Color[0] += Color2[0]*obj.mat.reflect
            Color[1] += Color2[1]*obj.mat.reflect
            Color[2] += Color2[2]*obj.mat.reflect
        if Refraction:
            if obj.mat.trans < 1.0:
                #Form the refraction ray.
                N = n
                L = VectorNegate(CastingRay.dir)
                if CastingRay.index == obj.mat.refract:
                    nlovernr = CastingRay.index / 1.0
                else:
                    nlovernr = 1.0 / obj.mat.refract
                if nlovernr == obj.mat.refract/1.0:
                    N = VectorNegate(N)
                ndotl = DotProduct(N,L)
                bracketquan = 1-(ndotl**2)
                undersqrt = 1 - ((nlovernr**2)*bracketquan)
                if undersqrt >= 0:
                    radical = sqrt(undersqrt)
                    pquan = ((nlovernr*ndotl) - radical)
                    t = VectorSubtract(ScalarVector(pquan,N),ScalarVector(nlovernr,L))
                    CastingRay2 = Ray(VectorAdd(CollisionPoint,ScalarVector(1,Normalize(t))),Normalize(t))
                    #Cast the reflection ray
                    Color2 = CastRay(CastingRay2)
                    #Add the reflection to the current color.
                    Color[0] += Color2[0]*(1.0-obj.mat.trans)
                    Color[1] += Color2[1]*(1.0-obj.mat.trans)
                    Color[2] += Color2[2]*(1.0-obj.mat.trans)
        if Lighting:
            #Change the luminosity of the color to be in relation to the light.
            LightIntensity = 0.0
            for light in Lights:
                direction = Normalize(VectorSubtract(light.pos,CollisionPoint))
                if Shadows:
                    CollisionPoint2 = VectorSubtract(CollisionPoint,direction)
                    ShadowRay = Ray(CollisionPoint2,direction)
                    Occluded = TrueFalseCastRay(ShadowRay)
                else:
                    Occluded = False
                if not Occluded:
                    Angle = degrees(acos(DotProduct(n,direction)))
                    if Angle > 90.0: Angle = 90.0
                    Intensity = (90.0-Angle)/90.0
                    LightIntensity += Intensity/float(len(Lights))
            Color[0] *= LightIntensity
            Color[1] *= LightIntensity
            Color[2] *= LightIntensity
    return Color
depth = 0
def main():
    #Globals
    global CameraPosition, RenderSurface, Objects, Lights, BackgroundColor
    global Lighting,Shadows,Reflection,Refraction
    global depth
    #Load Scene
    SceneData = SceneLoad.main()
    #Parametres
    Parametres = SceneData[0]
    RenderSize = Parametres[0]
    ViewBack = Parametres[1]
    CameraPosition = (RenderSize[0]/2.0,RenderSize[1]/2.0,-ViewBack)
    BackgroundColor = Parametres[2]
    #Light(s)
    Lights = SceneData[1]
    #Object(s)
    Objects = SceneData[2]
    #Lighting, Shadows, Reflection, Refraction
    Lighting,Shadows,Reflection,Refraction = SceneData[3]
    #Create Render Surface
    RenderSurface = pygame.Surface(RenderSize)
    RenderSurface.fill(BackgroundColor)
    #Render
    print("Beginning Render...")
    for y in range(RenderSize[1]):
        for x in range(RenderSize[0]):
            CastingRay = Ray(CameraPosition,(x-CameraPosition[0],y-CameraPosition[1],0-CameraPosition[2]))
            Color = CastRay(CastingRay)
            Color = (rndint(Color[0]),rndint(Color[1]),rndint(Color[2]))
            RenderSurface.set_at((x,RenderSize[1]-y),Color)
            depth = 0
        sys.stdout.write(  "Render "+str(rndint((float(y)/float(RenderSize[1]))*100.0))+"%        " + "\r"  )
        sys.stdout.flush()
    sys.stdout.write(  "Render 100%        " + "\r"  )
    sys.stdout.flush()
    print(" ")
    print("Render Complete!")
    #Return the Data
    return RenderSurface
                    



