#Use '#' to add comments.  Follow
#the formats apparent here.  Just
#don't leave space(s) after any
#line containing any data.

SIZE (256,128)
#SIZE (1024,512)
BACK 1700
BGCL (0,0,0)

LIGHT (-128,-64,0)
LIGHT (128,-64,0)
#LIGHT (-512,-256,0)
#LIGHT (512,-256,0)

SCALAR 1

SPHERE (0,0,300),100,Silver

SPHERE (-50,10,185),15,Gold
SPHERE (-30,-10,185),15,Gold
SPHERE (-10,-30,185),15,Gold

SPHERE (0,10,185),15,Gold
SPHERE (20,-10,185),15,Gold
SPHERE (40,-30,185),15,Gold

SPHERE (-5,-10,185),5,Mirror

#Leave this footer in.