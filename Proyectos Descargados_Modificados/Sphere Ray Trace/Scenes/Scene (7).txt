#Use '#' to add comments.  Follow
#the formats apparent here.  Just
#don't leave space(s) after any
#line containing any data.

#SIZE (256,128)
SIZE (1024,512)
BACK 1700
BGCL (0,0,0)

#LIGHT (300,0,-300)
#LIGHT (-300,0,-300)
LIGHT (1200,0,-1200)
LIGHT (-1200,0,-1200)

SCALAR 4

ROT -30Y
ROT 10X
SPHERE (20,20,20),15,Gold
SPHERE (20,20,-20),15,Gold
SPHERE (20,-20,20),15,Gold
SPHERE (20,-20,-20),15,Gold
SPHERE (-20,20,20),15,Gold
SPHERE (-20,20,-20),15,Gold
SPHERE (-20,-20,20),15,Gold
SPHERE (-20,-20,-20),15,Gold

#Leave this footer in.