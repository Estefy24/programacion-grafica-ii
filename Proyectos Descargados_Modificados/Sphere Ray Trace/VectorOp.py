#Revisado y Editado por: Joana Estefanía Nicolalde Perugachi
def ScalarVector(s,vec):
    return [s*vec[0],s*vec[1],s*vec[2]]
def VectorDivide(vec1,vec2):
    return [float(vec1[0])/float(vec2[0]),float(vec1[1])/float(vec2[1]),float(vec1[2])/float(vec2[2])]
def VectorAdd(vec1,vec2):
    return [vec1[0]+vec2[0],vec1[1]+vec2[1],vec1[2]+vec2[2]]
def VectorSubtract(vec1,vec2):
    return [vec1[0]-vec2[0],vec1[1]-vec2[1],vec1[2]-vec2[2]]
def DotProduct(vec1,vec2):
    return (vec1[0]*vec2[0])+(vec1[1]*vec2[1])+(vec1[2]*vec2[2])
